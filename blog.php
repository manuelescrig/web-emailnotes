<!DOCTYPE html>
<html>

<head>
    <!-- Header-Top -->
    <?php include 'header-top.php';?>

    <!-- Title -->
    <title>Blog | Digital Photo Frame App</title>

</head>

<!-- Body -->

<body>
    <!-- Header -->
    <header id="header-blog">
        <section class="hero">
            <!-- Hero head: will stick at the top -->
            <!-- Navigation-Bar -->
            <?php include 'navigation-bar.php';?>

            <div class="container my-5"></div>
            <!-- Hero content: will be in the middle -->
            <div class="hero-body"></div>

            <!-- Hero footer: will stick at the bottom -->
            <div class="hero-foot"></div>
        </section>
    </header>

    <section class="section column is-8 is-offset-2 m-2">

        <!-- Title -->
        <div class="container is-max-desktop has-text-centered my-6">
            <h6 class="header-eyebrow">NEWS</h6>
            <h3 class="header-title">Digital Photo Frame Blog</h3>
            <h2 class="header-description">
                Read about the latest updates and get the most out of your device.
            </h2>
        </div>
        <!-- /Title -->


        <nav class="level"></nav>

        <div class="container is-max-desktop px-6">
            <div class="is-divider"></div>
        </div>

        <!-- Post -->
        <div class="container">
            <a href="/how-to-lock-your-ipad-into-a-single-app.php" class="text-color">
                <picture>
                    <source srcset="/images/lock-your-ipad-into-a-single-app-6.webp" type="image/webp"
                        alt="Lock your iPad into a single App" />
                    <source srcset="/images/lock-your-ipad-into-a-single-app-6.png" type="image/png"
                        alt="Lock your iPad into a single App" />
                    <img src="/images/lock-your-ipad-into-a-single-app-6.png" alt="Lock your iPad into a single App"
                        class="post" />
                </picture>
            </a>

            <h2 class="post-title mt-5 mb-5">
                <a href="/how-to-lock-your-ipad-into-a-single-app.php">How to lock your iPad into a single App use</a>
            </h2>
            <p>
                Turn your iPad or iPhone into a single-use tool, whether temporarily
                or permanently. Lock your device to use a specific App. Prevent others
                from accessing other iPad Apps or changing its settings.
            </p>
            <h5 class="post-date mt-3 mb-3">April 20, 2020 by Manuel Escrig</h5>
            <div class="container is-max-desktop px-6">
                <div class="is-divider"></div>
            </div>
        </div>
        <!-- /Post -->

        <!-- Post -->
        <div class="container">
            <a href="/turn-ipad-into-digital-photo-frame.php" class="text-color">
                <picture>
                    <source srcset="/images/digital-photo-frame-app-baby.webp" type="image/webp"
                        alt="Turn iPad into a Digital Photo Frame" />
                    <source srcset="/images/digital-photo-frame-app-baby.png" type="image/png"
                        alt="Turn iPad into a Digital Photo Frame" />
                    <img src="/images/digital-photo-frame-app-baby.png" alt="Turn iPad into a Digital Photo Frame"
                        class="post" />
                </picture>
            </a>

            <h2 class="post-title mt-5 mb-5">
                <a href="/turn-ipad-into-digital-photo-frame.php">How to turn your iPad into a Digital Photo Frame</a>
            </h2>
            <p>
                Start taking advantage now of your old device and use it as a digital
                picture frame to revive your best life moments. It's pretty easy. Here
                we will explain to you how.
            </p>
            <h5 class="post-date mt-3 mb-3">May 1, 2019 by Manuel Escrig</h5>
            <div class="container is-max-desktop px-6">
                <div class="is-divider"></div>
            </div>
        </div>
        <!-- /Post -->

        <!-- Post -->
        <div class="container">
            <a href="/introducing-digital-photo-frame-app.php" class="text-color">
                <picture>
                    <source srcset="/images/about/Twins.webp" type="image/webp" alt="Introducing Digital Photo Frame" />
                    <source srcset="/images/about/Twins.png" type="image/png" alt="Introducing Digital Photo Frame" />
                    <img src="/images/about/Twins.png" alt="Introducing Digital Photo Frame" class="post" />
                </picture>
            </a>

            <h2 class="post-title mt-5 mb-5">
                <a href="/introducing-digital-photo-frame-app.php">Introducing Digital Photo Frame</a>
            </h2>
            <p>The story, read about how everything got started.</p>
            <h5 class="post-date mt-3 mb-3">April 2, 2019 by Manuel Escrig</h5>
        </div>
        <!-- /Post -->
    </section>

    <nav class="level"></nav>

    <!-- Footer-Top -->
    <?php include 'footer-top.php';?>

    <!-- Footer-Bottom -->
    <?php include 'footer-bottom.php';?>

</body>

</html>