<!DOCTYPE html>
<html>

<head>
    <!-- Header-Top -->
    <?php include 'header-top.php';?>

    <!-- Title -->
    <title>Privacy Policy | Digital Photo Frame App</title>

</head>

<!-- Body -->
<body>
    <!-- Header -->
    <header id="header-about">
        <section class="hero">
            <!-- Hero head: will stick at the top -->
            <!-- Navigation-Bar -->
            <?php include 'navigation-bar.php';?>

            <div class="container my-5"></div>
            <!-- Hero content: will be in the middle -->
            <div class="hero-body"></div>

            <!-- Hero footer: will stick at the bottom -->
            <div class="hero-foot"></div>
        </section>
    </header>

    <section class="section m-2">
        <!-- Title -->
        <div class="container is-max-desktop has-text-centered my-6">
            <h6 class="header-eyebrow">LEGAL</h6>
            <h3 class="header-title">Privacy Policy</h3>
            <h2 class="header-description">
            </h2>
        </div>
        <!-- /Title -->

        <nav class="level"></nav>

        <div class="container is-max-desktop px-6">
            <div class="is-divider"></div>
        </div>

        <!-- Body -->
        <div class="container has-text-justified is-max-desktop px-6">

            <p class="post-paragraph">
                Ventura Media built the Digital Photo Frame App as a Commercial app. This SERVICE is provided by Ventura
                Media and is intended for use as is.
                This page is used to inform website visitors regarding our policies with the collection, use, and
                disclosure of Personal Information if anyone decided to use our Service.
            </p>

            <p class="post-paragraph">
                If you choose to use our Service, then you agree to the collection and use of information in relation to
                this policy. The Personal Information that we collect is used for providing and improving the Service.
                We will not use or share your information with anyone except as described in this Privacy Policy.
                The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is
                accessible at Digital Photo Frame App unless otherwise defined in this Privacy Policy.
            </p>

            <h3 class="header-subtitle">Information Collection and Use</h3>
            <p class="post-paragraph">
                For a better experience, while using our Service, we may require you to provide us with certain
                personally identifiable information. The information that we request is will be retained by us and used
                as described in this privacy policy.
                The app does use third party services that may collect information used to identify you.
                Link to privacy policy of third party service providers used by the app
                - AppCenter
            </p>

            <h3 class="header-subtitle">Debug Data</h3>
            <p class="post-paragraph">
                We want to inform you that whenever you use the App, in a case of an error in the app we collect data
                and information (through third party products) on your phone called debug data. This debug data may
                include information such as your device Internet Protocol address, device name, operating system
                version, the configuration of the app when using the App, the time and date of your use of the Service,
                and other statistics.
            </p>

            <h3 class="header-subtitle">Service Providers</h3>
            <p class="post-paragraph">
                We may employ third-party companies and individuals due to the following reasons:
                - To facilitate our Service;
                - To provide the Service on our behalf;
                - To perform Service-related services; or
                - To assist us in analyzing how our Service is used.
                We want to inform users of this Service that these third parties have access to your Personal
                Information. The reason is to perform the tasks assigned to them on our behalf. However, they are
                obligated not to disclose or use the information for any other purpose.
            </p>

            <h3 class="header-subtitle">Security</h3>
            <p class="post-paragraph">
                We value your trust in providing us your Personal Information, thus we are striving to use commercially
                acceptable means of protecting it. But remember that no method of transmission over the internet, or
                method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.
            </p>

            <h3 class="header-subtitle">Links to Other Sites</h3>
            <p class="post-paragraph">
                This Service may contain links to other sites. If you click on a third-party link, you will be directed
                to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you
                to review the Privacy Policy of these websites. We have no control over and assume no responsibility for
                the content, privacy policies, or practices of any third-party sites or services.
            </p>

            <h3 class="header-subtitle">Changes to This Privacy Policy</h3>
            <p class="post-paragraph">
                We may update our Privacy Policy from time to time. Thus, you are advised to review this page
                periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on
                this page. These changes are effective immediately after they are posted on this page.
            </p>

            <h3 class="header-subtitle">Contact Us</h3>
            <p class="post-paragraph">
                If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.
            </p>


        </div>
        <!-- /Body -->

    </section>

    <!-- Footer-Top -->
    <?php include 'footer-top.php';?>

    <!-- Footer-Bottom -->
    <?php include 'footer-bottom.php';?>

</body>

</html>