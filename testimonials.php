<!DOCTYPE html>
<html>

<head>
    <!-- Header-Top -->
    <?php include 'header-top.php';?>

    <!-- Social -->
    <!-- Primary Meta Tags -->
    <title>Testimonials | Digital Photo Frame App - Photos & Videos Slideshow Player</title>
    <meta name="title" content="Testimonials | Digital Photo Frame App - Photos & Videos Slideshow Player">
    <meta name="description" content="This is the type of App that Apple should put on every iPad. I love that I can display photos from my daughter who live miles away.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://digitalphotoframeapp.com/">
    <meta property="og:title" content="Digital Photo Frame App - Photos & Videos Slideshow Player">
    <meta property="og:description" content="This is the type of App that Apple should put on every iPad. I love that I can display photos from my daughter who live miles away.">
    <meta property="og:image" content="https://digitalphotoframeapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta property="fb:app_id" content="519330621467436" />

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://digitalphotoframeapp.com/">
    <meta property="twitter:title" content="Digital Photo Frame App - Photos & Videos Slideshow Player">
    <meta property="twitter:description" content="This is the type of App that Apple should put on every iPad. I love that I can display photos from my daughter who live miles away.">
    <meta property="twitter:image" content="https://digitalphotoframeapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta name="twitter:site" content="@DigitalFrameApp">
    <meta name="twitter:image:alt" content="Digital Photo Frame App for iPad">

</head>

<!-- Body -->

<body>

    <!-- Header -->
    <header id="header-help">
        <section class="hero">
            <!-- Hero head: will stick at the top -->
            <!-- Navigation-Bar -->
            <?php include 'navigation-bar.php';?>

            <div class="container my-5"></div>
            <!-- Hero content: will be in the middle -->
            <div class="hero-body"></div>

            <!-- Hero footer: will stick at the bottom -->
            <div class="hero-foot"></div>
        </section>
    </header>

    <section class="section column is-8 is-offset-2 m-2">
        <!-- Title -->
        <div class="container is-max-desktop has-text-centered my-6">
            <h6 class="header-eyebrow">TESTIMONIALS</h6>
            <h3 class="header-title">They love using Digital Photo Frame App</h3>
            <h2 class="header-description">
                What users are saying.
            </h2>
        </div>
        <!-- /Title -->

        <nav class="level"></nav>

        <div class="container is-max-desktop px-6">
            <div class="is-divider"></div>
        </div>

        <div class="columns mb-6">
            <div class="column">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <h4 class="text-gradient">Perfect picture slideshow viewer</h4>
                <p>“This is the type of program that Apple should have baked
                    into their photos. This works with all of my pix or
                    individual folders, and many other options with other
                    interesting picture feeds which are great too as options.
                    What I find especially valuable is that I can vary the
                    time the slide is up (I often project them on my television set)
                    from short to several minutes per picture if desired,
                    not just the 20 second maximum that Apple gives you.
                    I think the main improvement I would make is that when
                    looking for an individual folder of my pictures within the
                    app, they don’t seem to be organized in any particular
                    order so quite a bit of searching can be required if you have
                    lots of pic folders. But other than that I think it’s perfect.”
                    <small>CareyAndrew</small>
                </p>
            </div>
            <div class="column">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <h4 class="text-gradient">BEST PHOTO SLIDESHOW APP!</h4>
                <p>"My husband bought this as a mother’s day gift and I absolutely LOVE it. 
                It allows you, to pick a shared album and it automatically refreshes when new photos
                or videos are added! It’s amazing to see pics pop in from my grandchildren."
                    <small>Gingersdogmom</small>
                </p>
            </div>
            <div class="column">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <h4 class="text-gradient">Uniquely excellent</h4>
                <p>"I’ve been searching for an app that does what
                    app does for literally years. This is the only one
                    I’ve found that a) can shuffle your entire photo
                    library ready for slide showing even though it is
                    on iCloud and b) allows you to have a slide show
                    where the transition time is completely adjustable
                    e.g you can set it to transition after 20 minutes
                    if you like so it acts like a proper photo frame.
                    Other apps I’ve tried struggle with the uploading
                    from the iCloud, frequently just locking up.
                    This one works all the time. Very impressive.”
                    <small>Adamrm1002</small>
                </p>
            </div>
        </div>

        <div class="columns mb-6">

        <div class="column">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <h4 class="text-gradient">Best of the Photo Frame Apps</h4>
                <p>"Tried several others, till I found this one.
                    This does everything it promises. Many other apps
                    like this crashed after a few minutes of use. I
                    love that I can pick as many of my albums
                    that I want to put on shuffle, and enjoy for hours
                    at a time."
                    <small>Rob99Review</small>
                </p>
            </div>
            <div class="column">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <h4 class="text-gradient">Took photos from everywhere</h4>
                <p>“It took my pictures from all the places on my phone
                    including WhatsApp, grouped them and allowed me to
                    “showcase” them. A great app.”
                    <small>Maagskop1997</small>
                </p>
            </div>
            <div class="column">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <h4 class="text-gradient">Worth it</h4>
                <p>“I hesitated before I bought this app considering 2 of
                    the reviews but went ahead and have been really happy.
                    I did not have any crashes. A lot of options in settings
                    and I am able to use it to show photos to customers in a
                    timely fashion.”
                    <small>JuliethRose</small>
                </p>
            </div>
        </div>

        <div class="container">
            <div class="is-divider-full-width"></div>
        </div>
    </section>

    <!-- Section Promotional -->
    <?php include 'call-to-action.php';?>
    <!-- /Section Promotional -->

    <!-- Footer-Top -->
    <?php include 'footer-top.php';?>

    <!-- Footer-Bottom -->
    <?php include 'footer-bottom.php';?>

</body>

</html>