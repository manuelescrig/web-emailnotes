<!DOCTYPE html>
<html>

<head>
    <!-- Header-Top -->
    <?php include 'header-top.php';?>

    <!-- Title -->
    <title>Thank You | Digital Photo Frame App</title>

</head>

<!-- Body -->

<body>

    <!-- Header -->
    <header id="header-help">
        <section class="hero">
            <!-- Hero head: will stick at the top -->
            <!-- Navigation-Bar -->
            <?php include 'navigation-bar.php';?>

            <div class="container my-5"></div>
            <!-- Hero content: will be in the middle -->
            <div class="hero-body"></div>

            <!-- Hero footer: will stick at the bottom -->
            <div class="hero-foot"></div>
        </section>
    </header>

    <section class="section m-4">
        <!-- Title -->
        <div class="container is-max-desktop has-text-centered my-6">
            <h6 class="header-eyebrow">CONTACT FORM</h6>
            <h3 class="header-title">Thank you!</h3>
            <h2 class="header-description">
                We'll get back to you in the next 48 hours.
            </h2>
        </div>
        <!-- /Title -->

        <nav class="level my-6"></nav>

    </section>

    <!-- Footer-Top -->
    <?php include 'footer-top.php';?>

    <!-- Footer-Bottom -->
    <?php include 'footer-bottom.php';?>

</body>

</html>