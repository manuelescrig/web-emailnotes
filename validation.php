<!DOCTYPE html>
<html>

<head>
    <!-- Header-Top -->
    <?php include 'header-top.php';?>

    <!-- Title -->
    <title>SPAM | Digital Photo Frame App</title>

</head>

<!-- Body -->

<body>

    <!-- Header -->
    <header id="header-help">
        <section class="hero">
            <!-- Hero head: will stick at the top -->
            <!-- Navigation-Bar -->
            <?php include 'navigation-bar.php';?>

            <div class="container my-5"></div>
            <!-- Hero content: will be in the middle -->
            <div class="hero-body"></div>

            <!-- Hero footer: will stick at the bottom -->
            <div class="hero-foot"></div>
        </section>
    </header>

    <section class="container m-4">
        <!-- Title -->
        <div class="section is-max-desktop has-text-centered my-6">
            <h6 class="header-eyebrow">CONTACT FORM</h6>
            <h3 class="header-title">SPAM</h3>
            <h2 class="header-description">
                Your message couldn't be submitted. You're trying to send some content that looks like SPAM.
            </h2>
        </div>
        <!-- /Title -->

        <nav class="level my-6"></nav>

    </section>

    <!-- Footer-Top -->
    <?php include 'footer-top.php';?>

    <!-- Footer-Bottom -->
    <?php include 'footer-bottom.php';?>

</body>

</html>