<!DOCTYPE html>
<html>

<head>
    <!-- Header-Top -->
    <?php include 'header-top.php';?>

    <!-- Social -->
    <!-- Primary Meta Tags -->
    <title>Introducing Digital Photo Frame | Digital Photo Frame App</title>
    <meta name="title" content="Introducing Digital Photo Frame App [Photos & Videos Slideshow Player]">
    <meta name="description" content="Turn your iPad or iPhone into a powerful Digital Photo Frame and start reliving the best memories of your life now.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://digitalphotoframeapp.com/introducing-digital-photo-frame-app/">
    <meta property="og:title" content="Introducing Digital Photo Frame App [Photos & Videos Slideshow Player]">
    <meta property="og:description" content="Turn your iPad or iPhone into a powerful Digital Photo Frame and start reliving the best memories of your life now.">
    <meta property="og:image" content="https://digitalphotoframeapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta property="fb:app_id" content="519330621467436" />

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://digitalphotoframeapp.com/introducing-digital-photo-frame-app/">
    <meta property="twitter:title" content="Introducing Digital Photo Frame App [Photos & Videos Slideshow Player]">
    <meta property="twitter:description" content="Turn your iPad or iPhone into a powerful Digital Photo Frame and start reliving the best memories of your life now.">
    <meta property="twitter:image" content="https://digitalphotoframeapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta name="twitter:site" content="@DigitalFrameApp">
    <meta name="twitter:image:alt" content="Digital Photo Frame App for iPad">

</head>

<!-- Body -->

<body>
    <!-- Header -->
    <header id="header-about">
        <section class="hero">
            <!-- Hero head: will stick at the top -->
            <!-- Navigation-Bar -->
            <?php include 'navigation-bar.php';?>

            <div class="container my-5"></div>
            <!-- Hero content: will be in the middle -->
            <div class="hero-body"></div>

            <!-- Hero footer: will stick at the bottom -->
            <div class="hero-foot"></div>
        </section>
    </header>

    <section class="section column is-8 is-offset-2 m-2">
        <!-- Title -->
        <div class="container is-max-desktop my-6">
            <h6 class="header-eyebrow">BLOG</h6>
            <h3 class="header-title py-2">Introducing Digital Photo Frame</h3>
            <h2 class="header-description">
                The story, read about how everything got started.
            </h2>
            <h5 class="post-date pt-5">April 2, 2019 by Manuel Escrig</h5>
        </div>
        <!-- /Title -->

        <div class="container">
            <div class="is-divider-full-width"></div>
        </div>

        <!-- Story -->
        <div class="container is-max-desktop my-6">
            <figure>
                <picture>
                    <source srcset="/images/about/Manuel.webp" type="image/webp" alt="The twins arrived" />
                    <source srcset="/images/about/Manuel.png" type="image/png" alt="The twins arrived" />
                    <img src="/images/about/Manuel.png" alt="The twins arrived" class="post" />
                    <figcaption>Me holding my newly-born twins.</figcaption>
                </picture>
            </figure>
            <h2 class="post-title">Introducing Myself</h2>
            <p class="post-paragraph">
                Hi, my name is Manuel Escrig and I'm the person behind Digital Photo
                Frame App. I'm a solo entrepreneur trying to build a sustainable
                business by creating the best possible App for people who want to use
                their iPad devices to display and relive their memories.
            </p>

            <h2 class="post-title">Pregnancy 🤰</h2>
            <p class="post-paragraph">
                It all started a couple of years back in time when my wife Margo was
                pregnant for the first time (hint: she was pregnant with twins). I've
                always been a photography enthusiast but I was never intereted in
                displaying my photos. This time was different, I was going to be
                father and I was sure I was going to be taking a lot of pictures and I
                wanted my parents who were living in another country to be up-to-date
                and able to see our newly-born children grow up.
            </p>
            <p class="post-paragraph">
                That's when I started doing research and I couldn't find an App on the
                App Store or a hardware photo frame that could automatically
                synchronize with my iCloud gallery of photos. So as a Software
                Engineer, I decided to create one. I was definitely too optimistic, I
                thought that with the free time I had I could have something properly
                built in the time I had left.
            </p>

            <h2 class="post-title">The Twins</h2>
            <p class="post-paragraph">
                Moving forward a few months, the twins were about to be born and I
                didn't have the App as complete as I thought, so I had to
                re-prioritize tasks in order to launch it before they were born so my
                parents could have it on their old iPad in their living room and see
                how their first grand children develop.
            </p>

            <p class="post-paragraph">
                After cutting some features and not spending any time in marketing nor
                in preparation for the launch day,
                <b>the App was published on the App Store 😅</b>. Probably one of the
                worst App Store launches you could find. The important thing for me
                was that my parents could have it on their iPad in their living room
                displaying pictures from iCloud Shared Stream I created for the
                occasion.
            </p>

            <figure>
                <picture>
                    <source srcset="/images/about/Twins.webp" type="image/webp" alt="A couple of years after" />
                    <source srcset="/images/about/Twins.png" type="image/png" alt="A couple of years after" />
                    <img src="/images/about/Twins.png" alt="A couple of years after" class="post" />
                    <figcaption>Maverick and Malena on a trip to Cambodia.</figcaption>
                </picture>
            </figure>

            <h2 class="post-title">Memories</h2>
            <p class="post-paragraph">
                A couple of years later, the same
                <b>iCloud Shared Stream has more than 2000 pictures</b>, a lot of good
                memories and some not as good (our baby boy was born with a
                diaphragmatic hernia and had to spend some time in the hospital, now
                he is great). Looking at it now, it was definitely worth it to ship
                the App without all the capabilities I had wanted and keep improving
                it later when I had more time.
            </p>

            <h2 class="post-title">My Plan 📝</h2>
            <p class="post-paragraph">
                Now, I'm trying to build a sustainable business out of it. The last
                year I've been implementing many features requested by the users and
                I'm currently implementing more of them. I believe that by
                <b>listening to the users</b> we can together create a
                <b>best in class App</b> that can provide and tell many other stories
                around the world displaying their most precious moments.
            </p>

            <p class="post-paragraph">
                Thank you for reading ❤️
                <br />
                - Manuel Escrig
            </p>

            <div class="container">
                <div class="is-divider-full-width"></div>
            </div>

            <!-- Social Media links -->
            <a target="_blank"
                href="https://www.facebook.com/sharer/sharer.php?u=https://digitalphotoframeapp.com/introducing-digital-photo-frame-app.php"
                class="fb-xfbml-parse-ignore"><img class="share-facebook" src="/images/svg/facebook-f.svg"
                    alt="Share on Facebook" />
                Share on Facebook</a>
            |
            <a target="_blank"
                href="https://twitter.com/intent/tweet?text=Introducing Digital Photo Frame App. 🖼 https://digitalphotoframeapp.com/introducing-digital-photo-frame-app.php"
                class="twitter-share-button" data-size="large"><img class="share-twitter" src="/images/svg/twitter.svg"
                    alt="Share on Twitter" />
                Share on Twitter</a>
        </div>
        <!-- /Story -->

        <div class="container">
            <div class="is-divider-dots"></div>
        </div>

        <p class="read-more-post">
            Read More From <a href="/blog">Blog</a>.
        </p>

        <!-- Post -->
        <div class="container mb-6">
            <h2 class="next-post-title mt-5 mb-5">
                <a href="/turn-ipad-into-digital-photo-frame.php">How to turn your iPad into a Digital Photo Frame</a>
            </h2>
            <p>
                Start taking advantage now of your old device and use it as a digital
                picture frame to revive your best life moments. It's pretty easy. Here
                we will explain to you how.
            </p>
        </div>
        <!-- /Post -->

    </section>

    <!-- Footer-Top -->
    <?php include 'footer-top.php';?>

    <!-- Footer-Bottom -->
    <?php include 'footer-bottom.php';?>

</body>

</html>