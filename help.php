<!DOCTYPE html>
<html>

<head>
    <!-- Header-Top -->
    <?php include 'header-top.php';?>

    <!-- Social -->
    <!-- Primary Meta Tags -->
    <title>Help | Digital Photo Frame App - Photos & Videos Slideshow Player</title>
    <meta name="title" content="Help | Digital Photo Frame App - Photos & Videos Slideshow Player">
    <meta name="description" content="Turn your iPad or iPhone into a powerful Digital Photo Frame and start reliving the best memories of your life now.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://digitalphotoframeapp.com/">
    <meta property="og:title" content="Digital Photo Frame App - Photos & Videos Slideshow Player">
    <meta property="og:description" content="Turn your iPad or iPhone into a powerful Digital Photo Frame and start reliving the best memories of your life now.">
    <meta property="og:image" content="https://digitalphotoframeapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta property="fb:app_id" content="519330621467436" />

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://digitalphotoframeapp.com/">
    <meta property="twitter:title" content="Digital Photo Frame App - Photos & Videos Slideshow Player">
    <meta property="twitter:description" content="Turn your iPad or iPhone into a powerful Digital Photo Frame and start reliving the best memories of your life now.">
    <meta property="twitter:image" content="https://digitalphotoframeapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta name="twitter:site" content="@DigitalFrameApp">
    <meta name="twitter:image:alt" content="Digital Photo Frame App for iPad">

    <!-- Captcha -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

</head>

<!-- Body -->

<body>

    <!-- Header -->
    <header id="header-help">
        <section class="hero">
            <!-- Hero head: will stick at the top -->
            <!-- Navigation-Bar -->
            <?php include 'navigation-bar.php';?>

            <div class="container my-5"></div>
            <!-- Hero content: will be in the middle -->
            <div class="hero-body"></div>

            <!-- Hero footer: will stick at the bottom -->
            <div class="hero-foot"></div>
        </section>
    </header>

    <section class="section m-4">
        <!-- Title -->
        <div class="container is-max-desktop has-text-centered my-6">
            <h6 class="header-eyebrow">FAQ</h6>
            <h3 class="header-title">Help</h3>
            <h2 class="header-description">
                If you have any other questions, feel free to contact.
            </h2>
        </div>
        <!-- /Title -->

        <nav class="level"></nav>

        <div class="container is-max-desktop px-6">
            <div class="is-divider"></div>
        </div>

        <div class="container">

            <div class="columns">

                <div class="column is-5 is-offset-1">

                    <div class="container">
                        <h3 class="header-subtitle">Subscriptions</h3>

                        <details class="default">
                            <summary>How can I cancel my subscription?</summary>
                            <p>You can cancel your subscription anytime. Apple provides a great website explaining all
                                the
                                different ways to do it.</p>
                        </details>

                        <details class="default">
                            <summary>How much does it cost?</summary>
                            <p>The prices varies depending on the App Store country. In the USA App Store life time
                                access
                                has a
                                cost of $25.99 and the yearly subscription $9.99.</p>
                        </details>

                        <details class="default">
                            <summary>Why are you charging a subscription fee for Digital Photo Frame App?</summary>
                            <p>It's because free Apps don't make money, and therefore can't survive. I'm an indie
                                developer
                                working on this App and I do my best implementing new feature requests, resolving
                                issues,
                                providing support to users in order to create a product/service that provides the best
                                possible
                                experiences with the resources I have. My goal is to keep listening to the users and
                                keep
                                improving this App so it can be best option for the people looking for a Digital Photo
                                Frame
                                either is on iOS, Android, or the old school hardware digital photo frames.</p>
                        </details>
                    </div>

                    <div class="container">
                        <h3 class="header-subtitle">Slideshows</h3>

                        <details class="default">
                            <summary>What kind of information can I display when playing a slideshow?</summary>
                            <p>When playing a slideshow you can display relevant information about the weather and the
                                images or
                                video being displayed.
                                Weather-related you can choose to show temperature (Current temperature, minimum &
                                maximum
                                daily
                                temperature, and humidity), air quality, UV index, and current date and time.
                                Media related you can display (if available) location, creation date, IPTC caption or
                                filename,
                                and device name used to take the image.</p>
                        </details>
                    </div>

                    <div class="container mb-6">
                        <h3 class="header-subtitle">General</h3>

                        <details class="default">
                            <summary>What are the different image sources?</summary>
                            <p>Digital Photo Frame currently supports images from the camera roll, albums, smart albums,
                                moments, iCloud and shared photo streams, Google Photos, as well as public images from
                                Flickr
                                and Unsplash. If you would like another source, please send me an email.</p>
                        </details>

                        <details class="default">
                            <summary>Can I see my albums from iCloud?</summary>
                            <p>Yes, your iCloud albums will appear on the menu under the Albums section and you will be
                                able
                                to
                                explore the photos from any iCloud album or shared photo stream.</p>
                        </details>

                        <details class="default">
                            <summary>Can I set a timer to play the slideshow at a specific time?</summary>
                            <p>Yes, through the slideshow settings screen you can select the start time and the end time
                                and
                                the
                                slideshow will play with the selected album during that time.</p>
                        </details>

                        <details class="default">
                            <summary>Does the app work on the iPad or iPhone?</summary>
                            <p>Yes, the app is compatible with the Apple devices like the iPhone, iPad, iPod Touch and
                                it
                                requires iOS 9.3 or later to work.</p>
                        </details>

                        <details class="default">
                            <summary>Is the app in my language?</summary>
                            <p>The app is fully translated to English, German, Japanese, Portuguese, Russian, Simplified
                                Chinese, Spanish and Traditional Chinese. We're currently working on adding new
                                languages.
                                If
                                you would like the App in your language, please let us know and we'll prioritize it.</p>
                        </details>

                        <details class="default">
                            <summary>How can I prevent the device screen from going dark?</summary>
                            <p>The App specifically prevents the device screen from going dark. There are some known
                                issues
                                with
                                some combinations of some old devices and iOS versions. The solution to this is to set
                                it
                                manually in your Device Settings. You can do so by going to the Settings, Display and
                                Brightness, Auto-Lock, and set it to never. Some work email accounts may not give the
                                user
                                the
                                option to not lock that account. In that case, you may need to log out from that account
                                in
                                order to prevent screen dimming.</p>
                        </details>

                        <details class="default">
                            <summary>Can I suggest a new feature?</summary>
                            <p>Yes, most of the features that the product has have been requested by users. You can
                                contact
                                me
                                with a detailed explanation of what you would like the product to do.</p>
                        </details>

                        <details class="default">
                            <summary>How do I prevent others from switching Apps on the iPad?</summary>
                            <p>Use the following steps. See How to lock your iPad into a single App.</p>
                        </details>

                    </div>
                </div>

                <div class="column is-5 is-offset-1">
                    <h3 class="header-subtitle" id="contact-form">Get In Touch</h3>
                    <p>
                        Our FAQ page provides support on the majority of areas of the app and common
                        related questions. If you can’t find what you’re looking for, fill out the form below and
                        we'll get back to you.
                    </p>

                    <div class="my-6">

                        <?php
$emailTo = 'info@digitalphotoframeapp.com';
$site_url = 'https://www.digitalphotoframeapp.com';
$missing_from = "Please provide an email address";
$invalid_from = "Please provide a valid email address - like name@domain.com";
$missing_message = "Please insert some text in the message";
$could_not_send = "There was a problem while sending the email. Please try again a bit later.";

$from_errors = array();
$message_errors = array();
$sending_error = array();
function cleanEmail($email)
{
    return trim(strip_tags($email));
}
function validEmail($email)
{
    $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i";
    return preg_match($pattern, cleanEmail($email));
}
function verifyFrom($email)
{
    if (empty($email)) {
        array_push($from_errors, $missing_from);
    }
    if (!validEmail($email)) {
        array_push($from_errors, $invalid_from);
    }
    return count($from_errors) == 0;
}
function verifyMessage($message)
{
    if (empty($message)) {
        array_push($message_errors, $missing_message);
    }

    return count($message_errors) == 0;
}
function contains($str, array $arr)
{
    foreach ($arr as $a) {
        if (stripos($str, $a) !== false) {
            return true;
        }

    }
    return false;
}
if ($_POST) {
    $name = stripslashes(trim($_POST['name']));
    $email = stripslashes(trim($_POST['email']));
    $message = stripslashes(trim($_POST['message']));
    $spamWords = array("invest", "investment", "investing", "viagra", "girls for sex", "girl for the night", "girls for the night", "women for sex", "sexy girls", "direct gold", "profitable database", "bitcoin", "sex", "adult", "dating");
    
    // reCAPTCHA validation
    if (empty($_POST['g-recaptcha-response'])) {
        echo "<script type='text/javascript'>window.location.href = '/validation.php';</script>";
        exit();
    }
    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
        
        // Google secret API
        $secretAPIkey = '6LfOBNoUAAAAAHG_XmeaTRUzZJnTruOKPJ3mHbH9';

        // reCAPTCHA response verification
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretAPIkey.'&response='.$_POST['g-recaptcha-response']);

        // Decode JSON data
        $response = json_decode($verifyResponse);
        if(!$response->success) {
            echo "<script type='text/javascript'>window.location.href = '/validation.php';</script>";
            exit();           
        }
    }
    
    // SPAM validation
    if (contains($name, $spamWords)) {
        echo "<script type='text/javascript'>window.location.href = '/validation.php';</script>";
        exit();
    }

    if (contains($email, $spamWords)) {
        echo "<script type='text/javascript'>window.location.href = '/validation.php';</script>";
        exit();
    }

    if (contains($message, $spamWords)) {
        echo "<script type='text/javascript'>window.location.href = '/validation.php';</script>";
        exit();
    }
    
    if (verifyFrom($email) && verifyMessage($message)) {
        $cleanEmail = cleanEmail($email);
        $subject = 'Contact - digitalphotoframeapp.com';
        $body = '
                <strong>Name: </strong>' . $name . '<br />
                <strong>Email: </strong>' . $email . '<br />
                <strong>Message: </strong>' . nl2br($message) . '<br />
                ';

        $headers = "MIME-Version: 1.1" . PHP_EOL;
        $headers .= "Content-type: text/html; charset=utf-8" . PHP_EOL;
        $headers .= "Content-Transfer-Encoding: 8bit" . PHP_EOL;
        $headers .= "Date: " . date('r', $_SERVER['REQUEST_TIME']) . PHP_EOL;
        $headers .= "Message-ID: <" . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . '@' . $_SERVER['SERVER_NAME'] . '>' . PHP_EOL;
        $headers .= "From: " . "=?UTF-8?B?" . base64_encode($name) . "?=" . " <$email> " . PHP_EOL;
        $headers .= "Return-Path: $emailTo" . PHP_EOL;
        $headers .= "Reply-To: $cleanEmail" . PHP_EOL;
        $headers .= "X-Mailer: PHP/" . phpversion() . PHP_EOL;
        $headers .= "X-Originating-IP: " . $_SERVER['SERVER_ADDR'] . PHP_EOL;
        if (mail($emailTo, "=?utf-8?B?" . base64_encode($subject) . "?=", $body, $headers)) {
            echo "<script type='text/javascript'>window.location.href = '/thank-you.php';</script>";
            exit();
        } else {
            array_push($sending_errors, $could_not_send);
        }
    }
}
?>
                        <form action="#contact-form" method="post">
                            <fieldset>
                                <div class="field">
                                    <label class="label">Name</label>
                                    <div class="control" id="name-field">
                                        <input type="text" id="name" name="name"
                                            class="form-control form-control-silver" placeholder="Your name" required />
                                    </div>
                                </div>

                                <div class="field">
                                    <label for="email" class="label">Email</label>
                                    <div class="control" id="email-field">
                                        <input type="email" id="email" name="email"
                                            class="form-control form-control-silver" placeholder="Your email" required />
                                    </div>
                                </div>

                                <div class="field">
                                    <label for="message" class="label">Message</label>
                                    <div class="control" id="message-field">
                                        <textarea class="form-control form-control-silver" id="message" name="message"
                                            rows="12" placeholder="Your message" required></textarea>
                                    </div>
                                </div>

                                <div class="control">
                                    <div class="form-group">
                                        <div class="g-recaptcha"
                                            data-sitekey="6LfOBNoUAAAAAL9EGPGqdHVn_bByhSOocPoSiEWi">
                                        </div>
                                        <br />
                                        <input type="submit" class="btn" value="Send Message"></input>
                                    </div>
                                </div>
                    </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>

    <!-- Section Promotional -->
    <?php include 'call-to-action.php';?>
    <!-- /Section Promotional -->

    <!-- Footer-Top -->
    <?php include 'footer-top.php';?>

    <!-- Footer-Bottom -->
    <?php include 'footer-bottom.php';?>

</body>

</html>