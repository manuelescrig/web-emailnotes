<div class="hero-head">
    <nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="https://www.digitalphotoframeapp.com">
                <img alt="Digital Photo Frame" src="/images/logo.png" />
            </a>
            <a class="navbar-item has-text-weight-semibold" href="/">Digital Photo Frame</a>

            <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false"
                data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                <a href="/how-it-works/" class="navbar-item">How It Works</a>
                <a href="/help/" class="navbar-item">Help</a>
                <a href="/testimonials/" class="navbar-item">Testimonials</a>
                <a href="/about/" class="navbar-item">About</a>
                <a href="/blog/" class="navbar-item">Blog</a>
            </div>

            <div class="navbar-end">
                <div class="navbar-item">
                    <div class="buttons">
                        <div class="navbar-reviews">
                            <svg class="navbar-reviews-stars" width="15" height="14" viewBox="0 0 15 14"
                                xmlns="http://www.w3.org/2000/svg">
                                <title>Star</title>
                                <path
                                    d="M7.5 10.537l-4.408 3.03 1.52-5.129L.367 5.182l5.348-.139L7.5 0l1.785 5.043 5.348.14-4.245 3.255 1.52 5.13z"
                                    fill="#ffd200"></path>
                            </svg>
                            <svg class="navbar-reviews-stars" width="15" height="14" viewBox="0 0 15 14"
                                xmlns="http://www.w3.org/2000/svg">
                                <title>Star</title>
                                <path
                                    d="M7.5 10.537l-4.408 3.03 1.52-5.129L.367 5.182l5.348-.139L7.5 0l1.785 5.043 5.348.14-4.245 3.255 1.52 5.13z"
                                    fill="#ffd200"></path>
                            </svg>
                            <svg class="navbar-reviews-stars" width="15" height="14" viewBox="0 0 15 14"
                                xmlns="http://www.w3.org/2000/svg">
                                <title>Star</title>
                                <path
                                    d="M7.5 10.537l-4.408 3.03 1.52-5.129L.367 5.182l5.348-.139L7.5 0l1.785 5.043 5.348.14-4.245 3.255 1.52 5.13z"
                                    fill="#ffd200"></path>
                            </svg>
                            <svg class="navbar-reviews-stars" width="15" height="14" viewBox="0 0 15 14"
                                xmlns="http://www.w3.org/2000/svg">
                                <title>Star</title>
                                <path
                                    d="M7.5 10.537l-4.408 3.03 1.52-5.129L.367 5.182l5.348-.139L7.5 0l1.785 5.043 5.348.14-4.245 3.255 1.52 5.13z"
                                    fill="#ffd200"></path>
                            </svg>
                            <svg class="navbar-reviews-stars" width="15" height="14" viewBox="0 0 15 14"
                                xmlns="http://www.w3.org/2000/svg">
                                <title>Star</title>
                                <path
                                    d="M7.5 10.537l-4.408 3.03 1.52-5.129L.367 5.182l5.348-.139L7.5 0l1.785 5.043 5.348.14-4.245 3.255 1.52 5.13z"
                                    fill="#ffd200"></path>
                            </svg>
                            <a href="/testimonials/" class="smooth-scroll">305 Reviews</a>
                        </div>

                        <a target="_blank"
                            href="https://itunes.apple.com/us/app/digital-photo-frame-pro-slideshow-creator/id1219786089?ls=1&mt=8"
                            class="">
                            <button class="button is-rounded button-color">
                                <img class="svg" src="/images/svg/apple.svg" alt="Download Digital Photo Frame App"
                                    height="13" width="13" />
                                Download Now
                            </button>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>

<script type="text/javascript">
document.addEventListener('DOMContentLoaded', () => {

// Get all "navbar-burger" elements
const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

// Check if there are any navbar burgers
if ($navbarBurgers.length > 0) {

  // Add a click event on each of them
  $navbarBurgers.forEach( el => {
    el.addEventListener('click', () => {

      // Get the target from the "data-target" attribute
      const target = el.dataset.target;
      const $target = document.getElementById(target);

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      el.classList.toggle('is-active');
      $target.classList.toggle('is-active');

    });
  });
}

});
</script>