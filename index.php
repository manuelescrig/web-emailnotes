<!DOCTYPE html>
<html>

<head>
    <!-- Header-Top -->
    <?php include 'header-top.php';?>

    <!-- Social -->
    <!-- Primary Meta Tags -->
    <title>Email Me App - Quickly Email Yourself Notes</title>
    <meta name="title" content="Email Me App - Quickly Email Yourself Notes">
    <meta name="description" content="Email Yourself Notes. The fastest way to record your thoughts, links, to-dos, and follow-ups when you're on the go.">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://emailnotesapp.com/">
    <meta property="og:title" content="Email Me App - Quickly Email Yourself Notes">
    <meta property="og:description" content="Email Yourself Notes. The fastest way to record your thoughts, links, to-dos, and follow-ups when you're on the go.">
    <meta property="og:image" content="https://emailnotesapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta property="fb:app_id" content="519330621467436" />

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://emailnotesapp.com/">
    <meta property="twitter:title" content="Email Me App - Quickly Email Yourself Notes">
    <meta property="twitter:description" content="Email Yourself Notes. The fastest way to record your thoughts, links, to-dos, and follow-ups when you're on the go.">
    <meta property="twitter:image" content="https://emailnotesapp.com/images/social/Digital_Photo_Frame_App.png">
    <meta name="twitter:site" content="@EmailMeApp">
    <meta name="twitter:image:alt" content="Email Me App for iPhone">

    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "SoftwareApplication",
      "operatingSystem": "iOS",
      "applicationCategory": "Productivity",
      "name": "Email Me App",
      "description": "Email Yourself Notes. The fastest way to record your thoughts, links, to-dos, and follow-ups when you're on the go.",
      "url": "https://www.emailnotesapp.com",
      "logo": "https://emailnotesapp.com/images/logo.png",
      "image": [
        "https://www.emailnotesapp.com/images/social/Digital_Photo_Frame_App.png"
       ],
       "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "4.7",
        "ratingCount": "257"
      }
    }
    </script>
</head>

<!-- Body -->

<body>
    <!-- Header -->
    <header id="header">
        <section class="hero is-fullheight">
            <!-- Hero head: will stick at the top -->
            <!-- Navigation-Bar -->
            <?php include 'navigation-bar.php';?>

            <!-- Extra Space On Table/Mobile -->
            <nav class="level is-hidden-desktop pt-4 mt-4"></nav>

            <!-- Hero content: will be in the middle -->
            <div class="hero-body">
                <div class="container">
                    <div class="columns is-vcentered">
                        <div class="column is-vcentered">
                            <h6 class="hero-eyebrow">MEET</h6>
                            <h1 class="hero-title">Email Me App</h1>
                            <h2 class="hero-description mt-4 mb-6">
                                The fastest way to record your thoughts, to-dos, and follow-ups when you're on the go
                            </h2>
                            <a target="_blank"
                                href="https://itunes.apple.com/us/app/digital-photo-frame-pro-slideshow-creator/id1219786089?ls=1&mt=8"
                                class="">
                                <button class="button is-rounded button-color">
                                    <img class="svg" src="/images/svg/apple.svg" alt="Download Digital Photo Frame App"
                                        height="13" width="13" />
                                    Download Now
                                </button>
                            </a>
                        </div>
                        <div class="column">
                            <img alt="Digital Photo Frame" src="/images/hero/digital-photo-frame-ipad.png" />
                        </div>
                    </div>
                </div>
            </div>

            <!-- Hero footer: will stick at the bottom -->
            <div class="hero-foot"></div>
        </section>
    </header>

    <!-- Testimonial -->
    <section class="section section-testimonial is-small has-text-centered is-bold my-6">
        <div class="container has-text-centered py-2">
            <div class="hero-review-stars">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
                <img class="star" src="/images/svg/star.svg" alt="Star">
            </div>
            <h5 class="hero-review">
                <blockquote>
                    <p class="quotation mx-6 my-3">
                        I love that I can display photos from my daughter's
                        shared stream and feel connected to my grandchildren miles away.
                        This is the type of App that Apple should put on every iPad.</p>
                    <h5 class="hero-testimonial">- App Store Review</h5>
                </blockquote>
            </h5>
        </div>
    </section>

    <section class="section section-c">
        <!-- Hero content: will be in the middle -->
        <div class="hero-body py-0">
            <div class="container">
                <div class="columns is-vcentered">
                    <div class="column is-vcentered">
                        <img alt="Digital Photo Frame" src="/images/hero/digital-photo-frame-gift.png" />
                    </div>
                    <div class="column is-vcentered">
                        <h6 class="hero-eyebrow">RELIVE</h6>
                        <h1 class="hero-subtitle">Precious Moments</h1>
                        <h2 class="hero-description mt-4 mb-5">
                            Extend the life of your iPad to serve as a reminder of the best
                            moments of your life.
                        </h2>
                        <h2 class="hero-description mt-4 mb-5">
                            Supports Photos, Live Photos & Videos.¹
                        </h2>

                        <div class="columns is-vcentered">
                            <div class="column is-2 has-text-centered">
                                <img class="hero-description-svg" src="/images/svg/support-photo.svg"
                                    alt="Supports Photos">
                                <h3 class="hero-subdescription">
                                    Photos
                                </h3>
                            </div>
                            <div class="column is-2 has-text-centered">
                                <img class="hero-description-svg" src="/images/svg/support-livephoto.svg"
                                    alt="Supports Live Photos">
                                <h3 class="hero-subdescription">
                                    Live Photos
                                </h3>
                            </div>
                            <div class="column is-2 has-text-centered">
                                <img class="hero-description-svg" src="/images/svg/support-video.svg"
                                    alt="Supports Videos">
                                <h3 class="hero-subdescription">
                                    Video <span class="tag is-rounded">NEW</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section class="section section-d">
        <!-- Hero content: will be in the middle -->
        <div class="hero-body py-0">
            <div class="container">
                <div class="columns is-vcentered">
                    <div class="column is-vcentered">
                        <h6 class="hero-eyebrow">SNAP. SHARE. DISPLAY</h6>
                        <h1 class="hero-subtitle">Stay Connected</h1>
                        <h2 class="hero-description mt-4 mb-5">
                            Stay up-to-date with the people who matter most.
                        </h2>
                        <h2 class="hero-description mt-4 mb-5">
                            Display a shared stream and new photos and videos added to the stream will transition in
                            automatically. </h2>
                        </h2>
                    </div>
                    <div class="column">
                        <img alt="Digital Photo Frame" src="/images/hero/digital-photo-frame-connected.png" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-e">
        <!-- Hero content: will be in the middle -->
        <div class="hero-body py-0">
            <div class="container">
                <div class="columns is-vcentered">
                    <div class="column is-vcentered">
                        <img alt="Digital Photo Frame" src="/images/hero/digital-photo-frame-wedding.png" />
                    </div>
                    <div class="column is-vcentered">
                        <h6 class="hero-eyebrow">HAPPINESS STARTS HERE</h6>
                        <h1 class="hero-subtitle">Simply Powerful</h1>
                        <h2 class="hero-description mt-4 mb-5">
                            Display photo information such as location, date, camera & description.
                        </h2>
                        <h2 class="hero-description mt-4 mb-5">
                            Show weather, calendar, UV Index and AQI on-screen.
                        </h2>
                        <div class="columns is-vcentered">
                            <div class="column is-2 has-text-centered">
                                <img class="hero-description-svg" src="/images/svg/support-calendar.svg"
                                    alt="Display Calendar" />
                                <h3 class="hero-subdescription">
                                    Calendar
                                </h3>
                            </div>
                            <div class="column is-2 has-text-centered">
                                <img class="hero-description-svg" src="/images/svg/support-weather.svg"
                                    alt="Display Weather">
                                <h3 class="hero-subdescription">
                                    Weather
                                </h3>
                            </div>
                            <div class="column is-2 has-text-centered">
                                <img class="hero-description-svg" src="/images/svg/support-aqi.svg" alt="Display AQI">
                                <h3 class="hero-subdescription">
                                    Air Quality
                                </h3>
                            </div>
                            <div class="column is-2 has-text-centered">
                                <img class="hero-description-svg" src="/images/svg/support-uv-index.svg"
                                    alt="Display UV Index">
                                <h3 class="hero-subdescription">
                                    UV Index
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section class="section section-f">
        <!-- Hero content: will be in the middle -->
        <div class="hero-body py-0">
            <div class="container">
                <div class="columns is-vcentered">
                    <div class="column is-vcentered">
                        <h6 class="hero-eyebrow">CURATE</h6>
                        <h1 class="hero-subtitle">Your Memories</h1>
                        <h2 class="hero-description mt-4 mb-5">
                            Optimized for a large number of images.
                        </h2>
                        <h2 class="hero-description mt-4 mb-6">
                            A professional way to showcase your images to your clients.
                        </h2>
                        <a href="/how-it-works" class="">
                            <button class="button is-rounded button-color">
                                See How It Works
                            </button>
                        </a>

                    </div>
                    <div class="column">
                        <img alt="Digital Photo Frame" src="/images/hero/digital-photo-frame-flowers.png" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-enjoy">
        <div class="container has-text-centered pt-6">
            <h6 class="hero-eyebrow">ENJOY</h6>
            <h1 class="hero-subtitle">All your Photos & Videos</h1>
            <h2 class="hero-description mt-4 mb-5">
                Display your iPad Photo Library, Google Photos, Flickr & Unsplash.
            </h2>
            <div class="container sources">
                <div class="columns is-vcentered">
                    <div class="column has-text-centered">
                        <img class="sources-svg" src="/images/sources/apple-photos.png" alt="Apple Photos">
                    </div>
                    <div class="column has-text-centered">
                        <img class="sources-svg" src="/images/sources/google-photos.png" alt="Google Photos">
                    </div>
                    <div class="column has-text-centered">
                        <img class="sources-svg" src="/images/sources/flickr-photos.png" alt="Flickr Photos">
                    </div>
                    <div class="column has-text-centered">
                        <img class="sources-svg" src="/images/sources/unsplash-photos.png" alt="Unsplash Photos">
                    </div>
                </div>
            </div>
            <div class="columns is-vcentered py-6 my-6">
                <div class="column is-half is-offset-1">
                    <img alt="Digital Photo Frame" src="/images/hero/digital-photo-frame-landscape.png" />
                </div>
                <div class="column decoration is-4 is-offset-1 is-hidden-touch">
                    <img alt="Digital Photo Frame" src="/images/hero/flowers.png" />
                </div>
            </div>
    </section>

    <!-- Section Promotional -->
    <?php include 'call-to-action.php';?>
    <!-- /Section Promotional -->

    <!-- Footer-Top -->
    <?php include 'footer-top.php';?>

    <!-- Footer-Bottom -->
    <?php include 'footer-bottom.php';?>

</body>

</html>